FROM php:7.4-apache

RUN a2enmod rewrite
########################################################################################################################
# Defaults
########################################################################################################################
RUN apt-get update && apt-get install apt-file -y && apt-file update && apt-get install vim -y\
    && apt-get install -y libzip-dev git wget --no-install-recommends zip \
    && apt-get clean && apt-get install -y default-mysql-client \
    && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

RUN docker-php-ext-install pdo mysqli pdo_mysql


# disable default site
RUN a2dissite 000-default.conf
########################################################################################################################
# Install Symfony CLI
########################################################################################################################
RUN curl -1sLf 'https://dl.cloudsmith.io/public/symfony/stable/setup.deb.sh' | bash
RUN apt install symfony-cli


########################################################################################################################
# Installing phpmyadmin
########################################################################################################################
RUN apt update && \
wget https://files.phpmyadmin.net/phpMyAdmin/5.2.0/phpMyAdmin-5.2.0-all-languages.tar.gz && \
tar xvf phpMyAdmin-5.2.0-all-languages.tar.gz && \
mv phpMyAdmin-5.2.0-all-languages/ /usr/share/phpmyadmin && \
mkdir -p /var/lib/phpmyadmin/tmp && \
chown -R www-data:www-data /var/lib/phpmyadmin

# Config files
COPY docker/config.inc.php /usr/share/phpmyadmin/config.inc.php
COPY docker/phpmyadmin.conf /etc/apache2/conf-available/phpmyadmin.conf
RUN a2enconf phpmyadmin.conf

########################################################################################################################
# PHP INI
########################################################################################################################
# Configuration File (php.ini) Path => /usr/local/etc/php
COPY docker/php.ini /usr/local/etc/php/


########################################################################################################################
# Configuration files
########################################################################################################################
COPY docker/symcourse.ett.loc.conf /etc/apache2/sites-enabled/symcourse.ett.loc.conf
COPY docker/entrypoint.sh /entrypoint.sh
COPY ./symfony-course /var/www/html

WORKDIR /var/www/html/symfony-course

########################################################################################################################
# Installing composer
########################################################################################################################
ENV COMPOSER_ALLOW_SUPERUSER 1
ENV COMPOSER_HOME /tmp
ENV COMPOSER_VERSION 2.0.6

RUN curl --silent --fail --location --retry 3 --output /tmp/installer.php --url https://raw.githubusercontent.com/composer/getcomposer.org/cb19f2aa3aeaa2006c0cd69a7ef011eb31463067/web/installer \
    && php -r " \
    \$signature = '48e3236262b34d30969dca3c37281b3b4bbe3221bda826ac6a9a62d6444cdb0dcd0615698a5cbe587c3f0fe57a54d8f5'; \
    \$hash = hash('SHA384', file_get_contents('/tmp/installer.php')); \
    if (!hash_equals(\$signature, \$hash)) { \
    unlink('/tmp/installer.php'); \
    echo 'Integrity check failed, installer is either corrupt or worse.' . PHP_EOL; \
    exit(1); \
    }" \
    && php /tmp/installer.php --no-ansi --install-dir=/usr/bin --filename=composer --version=${COMPOSER_VERSION} \
    && composer --ansi --version --no-interaction \
    && rm -rf /tmp/* /tmp/.htaccess


WORKDIR /var/www/html/symfony-course

RUN chmod +x /entrypoint.sh
CMD ["apache2-foreground"]
ENTRYPOINT ["/entrypoint.sh"]
