build:
	docker-compose up --build -d

up:
	docker-compose up -d

# stop:
# 	docker stop $(docker ps -a -q) && docker system prune -af --volumes

web:
	docker exec -it php-web bash

mysql:
	docker exec -it mysql-web bash
