# Install Docker and create container
* PHP 7.4
* MySQL 8

```sh
docker-compose up --build -d
```

or

```sh
make build
```

# Login into Docker image

```sh
docker exec -it <image name> bash
```

# On Docker

```sh
composer create-project symfony/skeleton:"^5.4" symfony-course 
```

Check [Symfony Local](http://localhost/symfony-course/public/)

# Symfony project forder structure

symfony-course
    |_
        |_ bin    - for unit tests
        |_ config - configuration files
        |_ public - server looks here and reads index.php
        |_ src    - here a programmer makes all files, e.g. controllers, entities etc.
        |_ var    - cache files, sessions etc.
        |_ vendor - forlder for external php libraries
        
# Check requirements (run on docker from project directory)

[PHP manual](https://www.php.net/manual/en/opcache.installation.php#opcache.installation.bundled)

[Symfony manual](https://symfony.com/doc/current/performance.html#use-a-byte-code-cache-e-g-opcache)

# Final environment:

```sh
php bin/console about
```

 -------------------- --------------------------------- 
  Symfony                                               
 -------------------- --------------------------------- 
  Version              5.4.12                           
  Long-Term Support    Yes                              
  End of maintenance   11/2024 (in +823 days)           
  End of life          11/2025 (in +1188 days)          
 -------------------- --------------------------------- 
  Kernel                                                
 -------------------- --------------------------------- 
  Type                 App\Kernel                       
  Environment          dev                              
  Debug                true                             
  Charset              UTF-8                            
  Cache directory      ./var/cache/dev (604 KiB)        
  Build directory      ./var/cache/dev (604 KiB)        
  Log directory        ./var/log (0 B)                  
 -------------------- --------------------------------- 
  PHP                                                   
 -------------------- --------------------------------- 
  Version              7.4.30                           
  Architecture         64 bits                          
  Intl locale          n/a                              
  Timezone             UTC (2022-08-30T08:13:55+00:00)  
  OPcache              false                            
  APCu                 false                            
  Xdebug               false                            
 -------------------- ---------------------------------  

# Commit to gitlab

```sh
git init --initial-branch=main
git remote add origin https://gitlab.com/eliyahukoren/symfony-course.git
git add .
git commit -m "Initial commit"
git push -u origin main
```

# MOST IMPORTANT FILE is .env

In the packages folders coresponding to the environment define.
in the .env file APP_ENV = dev|prod|test

# Check available commands:

```sh
php bin/console
```

# Install missing Symfony packages

[Twig](https://packagist.org/packages/symfony/twig-bundle)

```sh
composer require symfony/twig-bundle
```

[DoctrineBundle](https://symfony.com/bundles/DoctrineBundle/current/installation.html)

```sh
composer require doctrine/doctrine-bundle
```

**Commands list:**

```sh
php bin/console list doctrine
```

**Help**

```sh
php bin/console make:controller --help
```

[Symfony MakerBundle](https://symfony.com/bundles/SymfonyMakerBundle/current/index.html)

```sh
composer require --dev symfony/maker-bundle
```

**Commands list:**

```sh
php bin/console list make
```

**Help**

```sh
php bin/console list make --help
```


# Create controller with make-bundle

```sh
php bin/console make:controller DefaultController
```


## License

MIT

**Free Software, Hell Yeah!**

[//]: # (EXAMPLE: These are reference links used in the body of this note and get stripped out when the markdown processor does its job. There is no need to format nicely because it shouldn't be seen. Thanks SO - http://stackoverflow.com/questions/4823468/store-comments-in-markdown-syntax)

   [dill]: <https://github.com/joemccann/dillinger>
   [git-repo-url]: <https://github.com/joemccann/dillinger.git>
